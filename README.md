# recipe-app-api-proxy

NGINX proxy app for our recipe app API

## Usage

### Enviroment Variables

* `LISTENT_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward to forware request to (default `app`)
* `APP_PORT` - Port of the app to forware requests to (default: `9000`)